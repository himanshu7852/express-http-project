function errorHandler(err,req,res,next){

    res.status(err.status).json({Message: err.message})
}
module.exports=errorHandler