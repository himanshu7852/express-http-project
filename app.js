const express = require('express')
const app = express()
const http=require('http')
const fs = require('fs')
const uuid = require('crypto')
const port = require('./config.js')
const path=require('path')
const router=express.Router()
const requestID=require('express-request-id')
const errorHandler=require('./errorHandler.js')

app.use(requestID());

app.use(function(req,res,next){
    fs.appendFile('logFiles.log',` url-${req.url} :: RequestID-${req.id}`+'\n',(err)=>{
        if(err){
            console.log(err)
        }
    })
    next()
})

router.get('/html', (req, res) => {
    res.status(200);

    res.sendFile(path.join(__dirname,'file1.html'))
})

router.get('/json', (req, res) => {
    res.status(200);
    
    res.sendFile(path.join(__dirname,'file.json'))
})

app.get('/uuid', (req, res) => {
    res.status(200);

    res.send(({"uuid": uuid.randomUUID()
}))
})

app.get('/status/:status', (req, res,next) => {
    const status = Number(req.params.status);
    let statusCode = http.STATUS_CODES[status];
    if (statusCode === undefined || status === undefined) {
        next({
            message:'Bad status request',
            status:404
        })
        // statusCode = 404;
        // res.send({"Status Code":`Bad status request`});
    }
    else {
        res.statusCode = Number(status);
        res.send({"status code": status});
    }
    
})
app.get('/delay/:time', (req, res,next) => {
    secondsDelay = Number(req.params.time)
    if (secondsDelay < 0 || isNaN(secondsDelay)) {
        res.statusCode=400
        // res.send({"Message":'Invalid Seconds'});
        next({
            message:'invalid seconds',
            status:400
        })
    }
    else {
        setTimeout(() => {
            res.send({"Responce after": `${secondsDelay} Seconds`})
        },secondsDelay * 1000);
    }
})
router.get('/log',(req,res)=>{
    
    fs.readFile('logFiles.log','utf-8',(err,data)=>{
        if(err)
        {
            console.log(err)
        }
        else{
            res.send(data)
        }
    })
})

app.use('/',router);

app.get('/', (req, res) => {
    res.send(`Homepage`)
})
app.use((req, res) => {
    res.status(400).send({"error": 'INVALID'})
})

app.use(errorHandler);

app.listen(port, () => {
    console.log(`${port} Server running`)
})